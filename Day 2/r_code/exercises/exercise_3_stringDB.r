# do you remember this:
# transcripts_annotated$t_sig_upreg
# it's where the information about the top upregulated transcripts is stored

# prepare a list
list_upreg_genes_for_string <- subset(transcripts_annotated, t_sig_upreg==1)

# map with string
map_upreg = string_db$map(list_upreg_genes_for_string, "ensembl_geneID", removeUnmappedRows = TRUE )

# extract interactions for downregulated proteins
string_interactions_upreg_genes <- string_db$get_interactions(map_upreg$STRING_id)
table(string_interactions_upreg_genes$combined_score >= 700)

# sort hits 
hits_up <- map_upreg[order(-map_upreg[,3]),]

# prep hits
hit_list_up = hits_up$STRING_id[1:100]

# plot the STRING network png  
string_db$plot_network(hit_list_up)

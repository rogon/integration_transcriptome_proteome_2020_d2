# Integration_transcriptome_proteome_2020_d2

Day 1 Introduction to Network Biology and Cytoscape
1. Introductory Lecture 9-10am Matt
2. 10:30 - Practical introduction with Livia
	- Introductory Cytoscape
		- interface
		- loading of networks into CS from files
		- loading of attributes into networks
		- merging networks
		- extracting subnetworks
		- editing attributes in CS node and edge table
		- layouts
		- manipulation of the style
		- apps/installation and launching

3. Lunch

4. Enrichment analysis, Clustering and String database access


Day 2 Integration of Transcriptome and Proteome Data


Section 1: Pre-process input files into formats needed for the analysis.

-   Load and clean up the datasets
-   Apply thresholds for significant hits

Section 2 BioMart and merging

-   Threshold data and add identifiers for quick data filtering
-   Annotate datasets using BioMaRt
-   Merge proteomic and transcriptomic datasets

Section 3 STRING

-   Extract known interactions for proteins using STRING database
-   String Plot interactions for top 100 significantly (<0.05) up-regulated
  proteins

Section 4 iGraph

- Process the STRING output into Cytoscape
  - Conversion to an iGraph object
  - Deleting edges and nodes
  - Converting to different formats e.g. edge lists, adjacency matrices.
  - Filtering using edge attributes e.g. combined score from String
  - Creating output files for further analysis

Section 5 iGraph to Cytoscape

-   Transferring networks to Cytoscape using the REST interface

Section 6 integration analysis in Cytoscape

- at this stage we will run Cytoscape to integrate and visualise datasets and 
  generate biologically relevant results.

